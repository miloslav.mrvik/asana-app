# Electron wrapper around Asana

This app is only a wrapper around Asana. It won't work while offline, because it uses the live Asana webpage. Its goal is to make easier to reach Asana because it is easier to hit `Super` (or `Cmd + space` on Mac) and type `asa<enter>` than searching the correct tab in browser.

# How to get it?

## Mac & Windows (& Linux)

You can download correct latest release on the [Releases page](//gitlab.com/miloslav.mrvik/asana-app/-/releases).

## Linux

Same as above + create your own `.desktop` file or maybe I will publish it as a linux package someday :)
