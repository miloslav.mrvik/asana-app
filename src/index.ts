import { app, BrowserWindow } from 'electron';
import packageJson from './package.json';
import path from 'path';
import { WindowStateKeeper } from './windowStateKeeper';
import type { WebPreferences } from 'electron/main';

if (!app.requestSingleInstanceLock()) {
    console.warn("Another instance is running! I am out.")
    app.quit();
} else {
    start();
}

function start() {

const webPreferences: WebPreferences = {
    partition: "persist:main",
    nodeIntegration: false,
    backgroundThrottling: true,
    navigateOnDragDrop: false,
    contextIsolation: true,
};

app.whenReady().then(() => {
    const stateKeeper = new WindowStateKeeper("main");
    const state = stateKeeper.getState();
    const win = new BrowserWindow({
        icon: path.resolve(__dirname, packageJson.mainWindow.icon),
        minHeight: packageJson.mainWindow.minHeight,
        minWidth: packageJson.mainWindow.minWidth,
        width: packageJson.mainWindow.width,
        height: packageJson.mainWindow.height,
        autoHideMenuBar: true,
        paintWhenInitiallyHidden: true,
        maximizable: true,
        minimizable: true,
        resizable: true,
        show: false,
        webPreferences,
        ...(state || {}),
    });

    if (state?.isMaximized) win.maximize();
    stateKeeper.track(win);

    win.loadURL(packageJson.mainWindow.url);

    win.on('close', (evt) => {
        evt.preventDefault();
        win.hide();
    });

    win.once('ready-to-show', () => {
        win.show();

        win.webContents.on("will-navigate", (evt, url) => {
            try {
                const u = new URL(url);
                if (!packageJson.mainWindow.allowedHostnames.includes(u.hostname.toLowerCase())) {
                    evt.preventDefault();
                    console.log("Navigation to popup", url);

                    const popup = new BrowserWindow({
                        icon: path.resolve(__dirname, packageJson.mainWindow.icon),
                        autoHideMenuBar: true,
                        parent: win,
                        modal: true,
                        type: "dialog",
                        webPreferences,
                    });

                    popup.loadURL(url).then(
                        () => {
                            const initWC = () => {
                                const onNavigation = (url: string) => {
                                    const u = new URL(url);
                                    console.log("Popup", url);
                                    if (packageJson.mainWindow.allowedHostnames.includes(u.hostname.toLowerCase())) {
                                        console.log("Popup to navigation", url);
                                        evt?.preventDefault();
                                        popup.webContents.removeAllListeners();
                                        popup.removeAllListeners();
                                        popup.close();
                                        win.loadURL(url);
                                    }
                                };

                                const wc = popup.webContents;
                                wc.on("will-navigate", (_, url) => {
                                    onNavigation(url);
                                });
                                wc.on("will-redirect", (_, url, __, isMainFrame) => {
                                    if (isMainFrame) onNavigation(url);
                                });
                            };
                            initWC();
                        }
                    );
                }
            } catch (err: unknown) {
                console.warn("Error while watching navigation");
            }
        })
    });

    app.on('window-all-closed', () => {
        app.quit()
    });

    app.on('activate', () => {
        win.show();
    });

    app.on('quit', () => {
        app.quit();
    });

    app.on('second-instance', (event, commandLine, workingDirectory) => {
        win.show();
    });
});

};