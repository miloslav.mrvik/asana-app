// main.js
import appConfig from 'electron-settings';
import * as z from 'zod';

const zWindowState = z.object({
    x: z.number().optional(),
    y: z.number().optional(),
    width: z.number().optional(),
    height: z.number().optional(),
    isMaximized: z.boolean(),
});

export type WindowState = z.infer<typeof zWindowState>

export class WindowStateKeeper {

    private window: null | Electron.BrowserWindow = null;
    private windowState: null | WindowState = null;
    private dirtyTimeoutId: null | NodeJS.Timeout = null;

    private readonly setStateDirty = () => {
        if (this.dirtyTimeoutId) clearTimeout(this.dirtyTimeoutId);
        const thisTimeout = this.dirtyTimeoutId = setTimeout(() => {
            if (this.dirtyTimeoutId !== thisTimeout) return;
            this.dirtyTimeoutId = null;
            this.updateState();
            this.saveState();
        }, 250);
    }

    constructor(public readonly windowName: string) {
        this.init();
    }

    public getState(): null | WindowState {
        const windowState = this.windowState;
        if (windowState) {
            return {
                ...windowState
            };
        }

        return null;
    }

    private init(): null | WindowState {
        const conf = appConfig.getSync(`windowState.${this.windowName}`);
        if (conf) {
            try {
                return this.windowState = zWindowState.parse(conf);
            } catch (err: unknown) {
                console.warn(`Failed to load window state ${this.windowName}`, err);
            }
        }

        // Default
        return this.windowState = null;
    }

    private async saveState(): Promise<void> {
        const windowState = await this.getState();
        const saveName = `windowState.${this.windowName}`;

        if (windowState) {
            await appConfig.set(saveName, windowState as any);
        } else {
            await appConfig.unset(saveName);
        }
    }

    public track(window: null | Electron.BrowserWindow): void {
        const events = ["move", "resize", "maximize"] as const;

        if (this.window) {
            for (const e of events) {
                // @ts-expect-error
                this.window.removeListener(e, this.setStateDirty);
            }
        }

        this.window = window;
        this.setStateDirty();

        if (this.window) {
            for (const e of events) {
                // @ts-expect-error
                this.window.on(e, this.setStateDirty);
            }
        }
    }

    public updateState(): void {
        const window = this.window;
        if (!window) {
            this.windowState = null;
        } else {
            const { x, y, width, height } = window.getNormalBounds();
            const isMaximized = window.isMaximized();
            this.windowState = {
                isMaximized, x, y, width, height
            } as WindowState;
        }
    }
}
